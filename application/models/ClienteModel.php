<?php 
include_once APPPATH. 'libraries/Pessoa.php';

class ClienteModel extends CI_Model{

    public function gera_tabela(){
        $html = '';
        $pessoa = new Pessoa();
        $data = $pessoa->lista();

        foreach ($data as $cliente) {
            $html .= '<tr>';
            $html .= '<td>'.$cliente['id'].'</td>';
            $html .= '<td>'.$cliente['nome'].'</td>';
            $html .= '<td>'.$cliente['sobrenome'].'</td>';
            $html .= '<td>'.$cliente['telefone'].'</td>';
            $html .= '<td>'.$cliente['mensagem'].'</td>';
            $html .= $this->action_buttons($cliente['id']);
            $html .= '</tr>';
        }
        return $html;
    }

    private function action_buttons($id){
        $html = '<td><a href="'.base_url('cliente/editar/'.$id). '">';
        $html .='<i class="fas fa-edit mr-2" title="editar"></i></a></td>';
        $html .= '<td><a href="'.base_url('cliente/deletar/'.$id). '">';
        $html .='<i class="fas fa-trash mr-2" title="editar"></i></a></td>';
        return $html;
    }

    public function detalhe($id) {
        $customer = new Customer();
        $custumer = $customer->lista();
        return $custumer[$id - 1];
    }

    public function salva_usuario(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('nome', 'Nome do Cliente', 'trim|required|min_length[2]|max_length[20]');
        $this->form_validation->set_rules('sobrenome', 'Sobrenome do Cliente', 'trim|required|min_length[3]|max_length[80]');
        $this->form_validation->set_rules('telefone', 'Telefone do Cliente', 'trim|required|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('mensagem', 'Mensagem do Cliente', 'trim|required|min_length[10]|max_length[255]');

        if($this->form_validation->run()){
          
            $data = $this->input->post();
            $pessoa = new Pessoa();
            $pessoa->cria_usuario($data);
            redirect('cliente/lista');
        }
    }

    public function edita_usuario($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $pessoa = new Pessoa();
        $pessoa->edita_usuario($data, $id);
        redirect('cliente/lista');

    }

    public function read($id){
        $pessoa = new Pessoa();
        return $pessoa->user_data($id);
    }

    public function deletar($id){
        $pessoa = new Pessoa();
        $pessoa->delete($id);
    }
}