<?php
include_once APPPATH. 'libraries/util/CI_Object.php';
    
class Pessoa extends CI_Object {

        private $data = array();
    
        public function lista(){

        $rs = $this->db->get('contato'); 
        $result = $rs->result_array();
        return $result;
		}

        public function cria_usuario($data){
            $this->db->insert('contato', $data);
        }

        public function user_data($id){

            $cond = array('id' => $id);
            $rs = $this->db->get_where('contato', $cond);
            return $rs->row_array();
        }

        public function edita_usuario($data, $id){
            $this->db->update('contato', $data, "id = $id");
        }

        public function delete($id){
            $cond = array('id' => $id);
            $this->db->delete('contato', $cond);
        }
}

