<?php 

class Cliente extends CI_Controller{
	
	public function index(){
		$this->load->view('common/header');
        $this->load->view('exemplo/navbar');
        $this->load->view('exemplo/carrosel');
        
        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');
	}

    public function sobrenos(){
		$this->load->view('common/header');
        $this->load->view('exemplo/navbar');
        $this->load->view('cliente/historia');
        $this->load->view('cliente/sobrenos');
        
    }
    
	public function produto(){
		$this->load->view('common/header');
        $this->load->view('exemplo/navbar');
        $this->load->view('cliente/andamento');
	}
	
    public function contato(){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');

        $this->load->model('ClienteModel', 'model');
        $this->model->salva_usuario();
        $this->load->view('cliente/form_cliente');

        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');
    }

	public function lista(){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');

        $this->load->model('ClienteModel', 'model');
        $data['tabela'] = $this->model->gera_tabela();
        $this->load->view('common/table', $data);

        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');
    }
	
    public function editar($id){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');

        //carregar os dados
        $this->load->model('ClienteModel', 'model');
        $data['user'] = $this->model->read($id);

        //exibir na pagina
        $this->load->view('cliente/form_cliente', $data);

        //modificar e salvar
        $this->model->edita_usuario($id);

        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');
    }

    public function deletar($id){
        $this->load->model('ClienteModel', 'model');
        $this->model->deletar($id);
        redirect('cliente/lista');
    }
	
}