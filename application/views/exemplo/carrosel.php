<section>
    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators">
        <li class="primary-color" data-target="#carousel-example-1z " data-slide-to="0" class="active"></li>
        <li class="primary-color" data-target="#carousel-example-1z" data-slide-to="1"></li>
        <li class="primary-color" data-target="#carousel-example-1z" data-slide-to="2"></li>
		<!-- <li class="primary-color" data-target="#carousel-example-1z" data-slide-to="3"></li> -->
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <img class="d-block w-100" style="background-size: cover; height:650;" src="<?= base_url('assets/mdb/img/banner.png')?>" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" style="background-size: cover; height:650;" src="<?= base_url('assets/mdb/img/banner1.png')?>" alt="Second slide">
        </div>
       
		<div class="carousel-item">
          <img class="d-block w-100" style="background-size: cover; height:650;" src="<?= base_url('assets/mdb/img/banner3.png')?>" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section>
  
