<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Elisom Auto Peças e Acessórios</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link href="<?= base_url('assets/mdb/css/bootstrap.min.css')?>" rel="stylesheet"> 
    <link href="<?= base_url('assets/mdb/css/mdb.min.css')?>" rel="stylesheet"> 
    <link href="<?= base_url('assets/css/style.css') ?> " rel="stylesheet">
	<link rel="shortcut icon"  href="assets/mdb/img/logoelisom.png">
  <link rel='stylesheet' id='compiled.css-css'  href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled-4.8.10.min.css?ver=4.8.10' type='text/css' media='all' />

  </head>

  <body>