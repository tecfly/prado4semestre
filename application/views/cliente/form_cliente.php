<div class="container mt-5">

<?php
echo validation_errors('<div class="alert alert-danger">', '</div>');?>
  
<form method="POST" class="mx-auto mb-3 col-md-6 text-center border border-light p-5" action="#!">

    <p class="h4 mb-4" style="font-weight:bold;">Formulário para Contato:</p>
    <input value="<?= isset($user) ? $user['nome'] : '' ?>"  type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">
    <input value="<?= isset($user) ? $user['sobrenome'] : '' ?>" type="text" id="sobrenome" name="sobrenome" class="form-control mb-4" placeholder="Sobrenome">
    <input value="<?= isset($user) ? $user['telefone'] : '' ?>" type="number" id="telefone" name="telefone" class="form-control mb-4" placeholder="Telefone">
    <textarea class="form-control rounded-0" value="<?= isset($user) ? $user['mensagem'] : '' ?>" type="text" id="mensagem" name="mensagem" class="form-control mb-4" rows="3" placeholder="Envie sua mensagem"></textarea>
    
    <button class="mt-3 btn btn-info  col-md-4" type="submit">Enviar</button>
</form>
</div>