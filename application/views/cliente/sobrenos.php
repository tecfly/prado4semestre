<div class=" mt-5 progress md-progress">
    <div class="progress-bar bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<div class="container mt-5">
<div class="col-md-12 col-sm-6 col-xs-6"><center><img style="max-width: 100%;" src="<?= base_url('assets/mdb/img/mv.png')?>"></center></div>
<div class="card-deck">

  <div class="card">
    <div class="card-footer bg-primary">
        <small class="text-muted"> </small>
    </div>
    <div class="card-body">
      <h5 class="card-title text-center" style="font-weight:bold; color:red;">Missão</h5>
      <p class="card-text text-justify" style="color:black; font-family:arial;">Fazer o atendimento com excelência em busca da satisfação do cliente.</p>
    </div>
    <div class="card-footer bg-primary">
    <small class="text-muted"> </small>
    </div>
  </div>
  <div class="card">
    <div class="card-footer bg-primary">
        <small class="text-muted"> </small>
    </div>
    <div class="card-body">
      <h5 class="card-title text-center" style="font-weight:bold; color:red;">Visão</h5>
      <p class="card-text text-justify" style="color:black; font-family:arial;">Ser referência no mercado de autopeças, como a melhor da região, buscando sempre um avanço tecnológico.</p>
    </div>
    <div class="card-footer bg-primary">
    <small class="text-muted"> </small>
    </div>
  </div>
  
  <div class="card">
    <div class="card-footer bg-primary">
        <small class="text-muted"> </small>
    </div>
    <div class="card-body">
      <h5 class="card-title text-center" style="font-weight:bold; color:red;">Valores</h5>
      <p class="card-text text-justify" style="color:black; font-family:arial;">Educação, Honestidade e Credibilidade.
</p>
    </div>
    <div class="card-footer bg-primary">
      <small class="text-muted"> </small>
    </div>
  </div>
</div>
</div>



